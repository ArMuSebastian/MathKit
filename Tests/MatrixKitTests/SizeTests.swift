import XCTest

final class SizeTests: XCTestCase {

    // MARK: - Different indices Hash into Set
    func testRowLessThanColumnIfHashedIntoSetCorrectManyDifferentSizes() {

        let amount = 1000
        let indices = (0..<amount)
            .map { TestableThings.Size.certain(rows: $0 - 1, columns: $0) }

        let set = Set(indices)

        XCTAssertTrue(set.count == amount)
    }

    func testRowGreaterThanColumnIfHashedIntoSetCorrectManyDifferentSizes() {

        let amount = 1000
        let indices = (0..<amount)
            .map { TestableThings.Size.certain(rows: $0 + 1, columns: $0) }

        let set = Set(indices)

        XCTAssertTrue(set.count == amount)
    }

    func testRowEqualColumnIfHashedIntoSetCorrectManyDifferentSizes() {

        let amount = 1000
        let indices = (0..<amount)
            .map { TestableThings.Size.certain(rows: $0, columns: $0) }

        let set = Set(indices)

        XCTAssertTrue(set.count == amount)
    }
    
    // MARK: - Same indices Hash into Set
    func testRowLessThanColumnIfHashedIntoSetCorrectManySameSizes() {

        let amount = 1000
        let indices = (0..<amount)
            .map { _ in TestableThings.Size.certain(rows: 0, columns: 1) }

        let set = Set(indices)

        XCTAssertTrue(set.count == 1)
    }

    func testRowGreaterThanColumnIfHashedIntoSetCorrectManySameSizes() {

        let amount = 1000
        let indices = (0..<amount)
            .map { _ in TestableThings.Size.certain(rows: 2, columns: 1) }

        let set = Set(indices)

        XCTAssertTrue(set.count == 1)
    }

    func testRowEqualColumnIfHashedIntoSetCorrectManySameSizes() {

        let amount = 1000
        let indices = (0..<amount)
            .map { _ in TestableThings.Size.certain(rows: 1, columns: 1) }

        let set = Set(indices)

        XCTAssertTrue(set.count == 1)
    }

    // MARK: - Row equal Column less
    func testRowEqualColumnsLessIfSizesEqual() {

        XCTAssertFalse(TestableThings.Size.certain(rows: 5, columns: 4) == TestableThings.Size.certain(rows: 5, columns: 5))
    }

    func testRowEqualColumnsLessIfSizesNotEqual() {

        XCTAssertTrue(TestableThings.Size.certain(rows: 5, columns: 4) != TestableThings.Size.certain(rows: 5, columns: 5))
    }

    // MARK: - Row equal Column greater
    func testRowEqualColumnsGreaterIfSizesEqual() {

        XCTAssertFalse(TestableThings.Size.certain(rows: 5, columns: 6) == TestableThings.Size.certain(rows: 5, columns: 5))
    }

    func testRowEqualColumnsGreaterIfSizesNotEqual() {

        XCTAssertTrue(TestableThings.Size.certain(rows: 5, columns: 6) != TestableThings.Size.certain(rows: 5, columns: 5))
    }

    // MARK: - Row less Column greater
    func testRowLessColumnsGreaterIfSizesEqual() {

        XCTAssertFalse(TestableThings.Size.certain(rows: 4, columns: 6) == TestableThings.Size.certain(rows: 5, columns: 5))
    }

    func testRowLessColumnsGreaterIfSizesNotEqual() {

        XCTAssertTrue(TestableThings.Size.certain(rows: 4, columns: 6) != TestableThings.Size.certain(rows: 5, columns: 5))
    }

    // MARK: - Row Greater Column Less
    func testRowGreaterColumnsLessfSizesEqual() {

        XCTAssertFalse(TestableThings.Size.certain(rows: 6, columns: 4) == TestableThings.Size.certain(rows: 5, columns: 5))
    }

    func testRowGreaterColumnsLessIfSizesNotEqual() {

        XCTAssertTrue(TestableThings.Size.certain(rows: 6, columns: 4) != TestableThings.Size.certain(rows: 5, columns: 5))
    }

    // MARK: - Row less Column Less
    func testRowLessColumnsLessIfSizesEqual() {

        XCTAssertFalse(TestableThings.Size.certain(rows: 4, columns: 4) == TestableThings.Size.certain(rows: 5, columns: 5))
    }

    func testRowLessColumnsLesslIfSizesNotEqual() {

        XCTAssertTrue(TestableThings.Size.certain(rows: 4, columns: 4) != TestableThings.Size.certain(rows: 5, columns: 5))
    }

    // MARK: - Row less Column equal
    func testRowLessColumnsEqualIfSizesEqual() {

        XCTAssertFalse(TestableThings.Size.certain(rows: 4, columns: 5) == TestableThings.Size.certain(rows: 5, columns: 5))
    }

    func testRowLessColumnsEqualIfSizesNotEqual() {

        XCTAssertTrue(TestableThings.Size.certain(rows: 4, columns: 5) != TestableThings.Size.certain(rows: 5, columns: 5))
    }

    // MARK: - Row greater Column greater
    func testRowGreaterColumnsGreaterIfSizesEqual() {

        XCTAssertFalse(TestableThings.Size.certain(rows: 6, columns: 6) == TestableThings.Size.certain(rows: 5, columns: 5))
    }

    func testRRowGreaterColumnsGreaterIfSizesNotEqual() {

        XCTAssertTrue(TestableThings.Size.certain(rows: 6, columns: 6) != TestableThings.Size.certain(rows: 5, columns: 5))
    }

    // MARK: - Row greater Column equal
    func testGreaterEqualColumnsEqualIfSizesEqual() {

        XCTAssertFalse(TestableThings.Size.certain(rows: 6, columns: 5) == TestableThings.Size.certain(rows: 5, columns: 5))
    }

    func testgreaterEqualColumnsEqualIfSizesNotEqual() {

        XCTAssertTrue(TestableThings.Size.certain(rows: 6, columns: 5) != TestableThings.Size.certain(rows: 5, columns: 5))
    }

    // MARK: - Row equal Column equal
    func testRowsEqualColumnsEqualIfSizesEqual() {

        XCTAssertTrue(TestableThings.Size.certain(rows: 5, columns: 5) == TestableThings.Size.certain(rows: 5, columns: 5))
    }

    func testRowsEqualColumnsEqualIfSizesNotEqual() {

        XCTAssertFalse(TestableThings.Size.certain(rows: 5, columns: 5) != TestableThings.Size.certain(rows: 5, columns: 5))
    }

}
