import MatrixKit

struct TestableThings {

    typealias TestPayload<T> = MatrixKit.Matrix<T>.Payload
    typealias TestMatrix<T> = MatrixKit.Matrix<T>
    typealias TestIndex = MatrixKit.Index
    typealias TestSize = MatrixKit.Size

}
