extension TestableThings {

    struct Matrix {

        typealias ThePayload<T> = TestableThings.TestPayload<T>
        typealias TheMatrix<T> = TestableThings.TestMatrix<T>
        typealias TheSize = TestableThings.TestSize

        private init() {}

        static func random<T: Randomable>() -> TheMatrix<T>? {

            return random(of: TestableThings.Size.random())
        }

        static func random<T: Randomable>(
            of size: TheSize
        ) -> TheMatrix<T>? {

            return fromPayload(payload: TestableThings.Payload.Correct.random(of: size))
        }

        static func fromPayload<T>(
            payload: ThePayload<T>
        ) -> TheMatrix<T>? {

            return matrix(payload: payload)
        }

        static func fromPayload<T>(
            payload: [T],
            rows: Int
        ) -> TheMatrix<T>? {

            return matrix(payload: payload, rows: rows)
        }

        static func fromPayload<T>(
            payload: [T],
            columns: Int
        ) -> TheMatrix<T>? {

            return matrix(payload: payload, columns: columns)
        }

    }

}

extension TestableThings.Matrix {

    private static func matrix<T>(
        payload: ThePayload<T>
    ) -> TheMatrix<T>? {

        return TheMatrix(array: payload)
    }

    private static func matrix<T>(
        payload: [T],
        rows: Int
    ) -> TheMatrix<T>? {

        return TheMatrix(array: payload, amountOfRows: rows)
    }

    private static func matrix<T>(
        payload: [T],
        columns: Int
    ) -> TheMatrix<T>? {

        return TheMatrix(array: payload, amountOfColumns: columns)
    }

}
