protocol Randomable {

    static func random() -> Self

}

extension Int: Randomable {

    static func random() -> Int {
        return Int.random(in: 10...99)
    }

}

extension String: Randomable {

    static func random() -> String {

        String(repeating: "a", count: Int.random(in: 1...15))
    }

}

