extension TestableThings {

    struct Size {

        typealias TheSize = TestableThings.TestSize

        private init() {}

        static func random() -> TheSize {
            return certain(rows: .random(), columns: .random())
        }

        static func certain(rows: Int, columns: Int) -> TheSize {
            return TheSize(rows: rows, columns: columns)
        }

    }

}
