extension TestableThings {

    struct Position {

        typealias TheIndex = TestableThings.TestIndex

        private init() {}

        static func random() -> TheIndex {
            return certain(row: .random(), column: .random())
        }

        static func certain(row: Int, column: Int) -> TheIndex {
            return index(row: row, column: column)
        }

        static func zero() -> TheIndex {
            return certain(row: 0, column: 0)
        }

        static func predefined1() -> TheIndex {
            return certain(row: 2, column: 5)
        }

    }

}

extension TestableThings.Position {

    private static func index(row: Int, column: Int) -> TheIndex {
        return TheIndex(row: row, column: column)
    }

}
