@testable import MatrixKit
import XCTest

final class MatrixTests: XCTestCase {

    // Creation
    func testMatrixCreateFromPayloadSuccess() {

        let payload = TestableThings.Payload.Correct.staticInt1()
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)

        XCTAssertTrue(matrix != nil)
    }

    func testMatrixPayloadCreateCorrect() {

        let payload = TestableThings.Payload.Correct.staticInt2()
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)

        XCTAssertTrue(matrix != nil)
    }

    func testMatrixCreateFromPayloadNoSuccess() {

        let payload = TestableThings.Payload.Incorrect.staticInt1()
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)

        XCTAssertTrue(matrix == nil)
    }

    func testMatrixCreateFromPayloadSemiSuccess() {

        let payload = TestableThings.Payload.Incorrect.staticInt2()
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)

        XCTAssertTrue(matrix == nil)
    }

    // Size
    func testMatrixHasPayloadSize1() {

        let payload = TestableThings.Payload.Correct.staticInt1()
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.size == TestableThings.Size.certain(rows: payload.count, columns: payload.first!.count))
    }

    func testMatrixHasPayloadSize2() {

        let payload = TestableThings.Payload.Correct.staticInt2()
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.size == TestableThings.Size.certain(rows: payload.count, columns: payload.first!.count))
    }

    func testMatrixHasPayloadSize3() {

        let size = TestableThings.Size.certain(rows: 3, columns: 2)
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.size == size)
    }

    func testMatrixHasPayloadSize4() {

        let size = TestableThings.Size.certain(rows: 3, columns: 3)
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.size == size)
    }

    func testMatrixHasPayloadSize5() {

        let size = TestableThings.Size.certain(rows: 3, columns: 4)
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.size == size)
    }

    func testMatrixHasPayloadSize6() {

        let size = TestableThings.Size.random()
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.size == size)
    }

    // MartixSize contains all MatrixIndices
    func testMatrixIndicesAreContainedByItsSize1() {

        let size = TestableThings.Size.certain(rows: 3, columns: 2)
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.indices.allSatisfy(matrix.size.contains(_:)))
    }

    func testMatrixIndicesAreContainedByItsSize2() {

        let size = TestableThings.Size.certain(rows: 3, columns: 3)
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.indices.allSatisfy(matrix.size.contains(_:)))
    }

    func testMatrixIndicesAreContainedByItsSize3() {

        let size = TestableThings.Size.certain(rows: 3, columns: 4)
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.indices.allSatisfy(matrix.size.contains(_:)))
    }

    func testMatrixIndicesAreContainedByItsSize4() {

        let size = TestableThings.Size.random()
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.indices.allSatisfy(matrix.size.contains(_:)))
    }

    // MartixPayloadSize contains all MatrixIndices
    func testMatrixIndicesAreContainedByItsPayloadSize1() {

        let size = TestableThings.Size.certain(rows: 3, columns: 2)
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.indices.allSatisfy(size.contains(_:)))
    }

    func testMatrixIndicesAreContainedByItsPayloadSize2() {

        let size = TestableThings.Size.certain(rows: 3, columns: 3)
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.indices.allSatisfy(size.contains(_:)))
    }

    func testMatrixIndicesAreContainedByItsPayloadSize3() {

        let size = TestableThings.Size.certain(rows: 3, columns: 4)
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.indices.allSatisfy(size.contains(_:)))
    }

    func testMatrixIndicesAreContainedByItsPayloadSize4() {

        let size = TestableThings.Size.random()
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let matrix = TestableThings.Matrix.fromPayload(payload: payload)!

        XCTAssertTrue(matrix.indices.allSatisfy(size.contains(_:)))
    }

    func testMatrixInitWorksSame() {

        let size = TestableThings.Size.random()
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let flattedPayload = payload.flatted
        let columnArraytMatrix = TestableThings.Matrix.fromPayload(payload: flattedPayload, columns: size.columns)!
        let rowArraytMatrix = TestableThings.Matrix.fromPayload(payload: flattedPayload, rows: size.rows)!
        let twoDimensionalArraytMatrix = TestableThings.Matrix.fromPayload(payload: payload)!

        let allMatix =
        [
            columnArraytMatrix,
            rowArraytMatrix,
            twoDimensionalArraytMatrix
        ]

        XCTAssertTrue(columnArraytMatrix == rowArraytMatrix)
        XCTAssertTrue(columnArraytMatrix == twoDimensionalArraytMatrix)
        XCTAssertTrue(rowArraytMatrix == twoDimensionalArraytMatrix)
        XCTAssertTrue(Set(allMatix).count == 1)
    }

    func testMatrixIndexReprresentCorrectElement() {

        let size = TestableThings.Size.random()
        let payload: TestableThings.Payload.TheIntPayload = TestableThings.Payload.Correct.random(of: size)
        let flattedPayload = payload.flatted
        let matrix = TestableThings.Matrix.fromPayload(payload: flattedPayload, columns: size.columns)!
        
        let indices = matrix.indices

        XCTAssertTrue(indices.count == flattedPayload.count)

        let indexAndValue = Array(zip(indices, flattedPayload))

        XCTAssertTrue(indexAndValue.allSatisfy { matrix[$0.0] == $0.1 })

    }


}
