import XCTest

final class IndexTests: XCTestCase {

}

// Positive row positive column
extension IndexTests {

    // MARK: - Different indices Hash into Set
    func testRowLessThanColumnIfHashedIntoSetCorrectManyDifferentIndices() {

        let amount = 1000
        let indices = (0..<amount)
            .map { TestableThings.Position.certain(row: $0, column: $0 + 1) }

        let set = Set(indices)

        XCTAssertTrue(set.count == amount)
    }

    func testRowGreaterThanColumnIfHashedIntoSetCorrectManyDifferentIndices() {

        let amount = 1000
        let indices = (0..<amount)
            .map { TestableThings.Position.certain(row: $0 + 1, column: $0) }

        let set = Set(indices)

        XCTAssertTrue(set.count == amount)
    }

    func testRowEqualColumnIfHashedIntoSetCorrectManyDifferentIndices() {

        let amount = 1000
        let indices = (0..<amount)
            .map { TestableThings.Position.certain(row: $0, column: $0) }

        let set = Set(indices)

        XCTAssertTrue(set.count == amount)
    }
    
    // MARK: - Same indices Hash into Set
    func testRowLessThanColumnIfHashedIntoSetCorrectManySameIndices() {

        let amount = 1000
        let indices = (0..<amount)
            .map { _ in TestableThings.Position.certain(row: 0, column: 1) }

        let set = Set(indices)

        XCTAssertTrue(set.count == 1)
    }

    func testRowGreaterThanColumnIfHashedIntoSetCorrectManySameIndices() {

        let amount = 1000
        let indices = (0..<amount)
            .map { _ in TestableThings.Position.certain(row: 2, column: 1) }

        let set = Set(indices)

        XCTAssertTrue(set.count == 1)
    }

    func testRowEqualColumnIfHashedIntoSetCorrectManySameIndices() {

        let amount = 1000
        let indices = (0..<amount)
            .map { _ in TestableThings.Position.certain(row: 1, column: 1) }

        let set = Set(indices)

        XCTAssertTrue(set.count == 1)
    }

    // MARK: - Row equal Column less
    func testRowEqualColumnLessForOneIndexNotGreaterThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 4) <= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnLessForOneIndexNotLessThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 4) >= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnLessForOneIndexLessThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 4) < TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnLessForOneIndexGreatedThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 4) > TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnLessForOneIndexEqualOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 4) == TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnLessForOneIndexNotEqualOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 4) != TestableThings.Position.certain(row: 5, column: 5))
    }

    // MARK: - Row equal Column greater
    func testRowEqualColumnGreaterForOneIndexNotGreaterThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 6) <= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnGreaterForOneIndexNotLessThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 6) >= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnGreaterForOneIndexLessThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 6) < TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnGreaterForOneIndexGreatedThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 6) > TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnGreaterForOneIndexEqualOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 6) == TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnGreaterForOneIndexNotEqualOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 6) != TestableThings.Position.certain(row: 5, column: 5))
    }

    // MARK: - Row less Column equal
    func testRowLessColumnEqualForOneIndexNotGreaterThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 4, column: 5) <= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnEqualForOneIndexNotLessThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 4, column: 5) >= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnEqualForOneIndexLessThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 4, column: 5) < TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnEqualForOneIndexGreatedThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 4, column: 5) > TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnEqualForOneIndexEqualOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 4, column: 5) == TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnEqualForOneIndexNotEqualOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 4, column: 5) != TestableThings.Position.certain(row: 5, column: 5))
    }

    // MARK: - Row greater Column equal
    func testRowGreaterColumnEqualForOneIndexNotGreaterThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 6, column: 5) <= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnEqualForOneIndexNotLessThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 6, column: 5) >= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnEqualForOneIndexLessThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 6, column: 5) < TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnEqualForOneIndexGreatedThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 6, column: 5) > TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnEqualForOneIndexEqualOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 6, column: 5) == TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnEqualForOneIndexNotEqualOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 6, column: 5) != TestableThings.Position.certain(row: 5, column: 5))
    }

    // MARK: - Row greater Column Less
    func testRowGreaterColumnLessForOneIndexNotGreaterThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 6, column: 4) <= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnLessForOneIndexNotLessThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 6, column: 4) >= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnLessForOneIndexLessThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 6, column: 4) < TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnLessForOneIndexGreatedThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 6, column: 4) > TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnLessForOneIndexEqualOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 6, column: 4) == TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowGreaterColumnLessForOneIndexNotEqualOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 6, column: 4) != TestableThings.Position.certain(row: 5, column: 5))
    }

    // MARK: - Row less Column greater
    func testRowLessColumnGreaterForOneIndexNotGreaterThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 4, column: 6) <= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnGreaterForOneIndexNotLessThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 4, column: 6) >= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnGreaterForOneIndexLessThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 4, column: 6) < TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnGreaterForOneIndexGreatedThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 4, column: 6) > TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnGreaterForOneIndexEqualOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 4, column: 6) == TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnGreaterForOneIndexNotEqualOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 4, column: 6) != TestableThings.Position.certain(row: 5, column: 5))
    }

    // MARK: - Row less Column less
    func testRowLessColumnLessForOneIndexNotGreaterThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 4, column: 4) <= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnLessForOneIndexNotLessThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 4, column: 4) >= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnLessForOneIndexLessThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 4, column: 4) < TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnLessForOneIndexGreatedThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 4, column: 4) > TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnLessForOneIndexEqualOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 4, column: 4) == TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowLessColumnLessForOneIndexNotEqualOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 4, column: 4) != TestableThings.Position.certain(row: 5, column: 5))
    }

    // MARK: - Row greater Column greater
    func testRowGreaterColumnGreaterForOneIndexNotGreaterThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 5) <= TestableThings.Position.certain(row: 4, column: 4))
    }

    func testRowGreaterColumnGreaterForOneIndexNotLessThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 5) >= TestableThings.Position.certain(row: 4, column: 4))
    }

    func testRowGreaterColumnGreaterForOneIndexLessThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 5) < TestableThings.Position.certain(row: 4, column: 4))
    }

    func testRowGreaterColumnGreaterForOneIndexGreatedThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 5) > TestableThings.Position.certain(row: 4, column: 4))
    }

    func testRowGreaterColumnGreaterForOneIndexEqualOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 5) == TestableThings.Position.certain(row: 4, column: 4))
    }

    func testRowGreaterColumnGreaterForOneIndexNotEqualOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 5) != TestableThings.Position.certain(row: 4, column: 4))
    }

    // MARK: - Row equal Column equal
    func testRowEqualColumnEqualForOneIndexNotGreaterThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 5) <= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnEqualForOneIndexNotLessThanOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 5) >= TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnEqualForOneIndexLessThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 5) < TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnEqualForOneIndexGreatedThanOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 5) > TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnEqualForOneIndexEqualOtherIndex() {

        XCTAssertTrue(TestableThings.Position.certain(row: 5, column: 5) == TestableThings.Position.certain(row: 5, column: 5))
    }

    func testRowEqualColumnEqualForOneIndexNotEqualOtherIndex() {

        XCTAssertFalse(TestableThings.Position.certain(row: 5, column: 5) != TestableThings.Position.certain(row: 5, column: 5))
    }

}
