//
//  Array+extension.swift
//  
//
//  Created by Artem Myshkin on 18.07.2021.
//

import Foundation

extension Array where Element: Collection {

    internal var flatted: [Element.Element] {
        self.flatMap { $0 }
    }

    internal var isAllChildrenHasSameCount: Bool {
        Set(self.map(\.count)).count == 1
    }

    internal var isValidMatrixPayload: Bool {
        self.isNotEmpty
        &&
        self.isAllChildrenHasSameCount
        &&
        (self.first?.isNotEmpty ?? false)
    }

}

extension Collection {

    internal var isNotEmpty: Bool {
        !isEmpty
    }

}

extension Array {

    internal func isFullyChunked(by size: Int) -> Bool {
        self.count.isMultiple(of: size)
    }

    internal func chunked(by size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }

}
