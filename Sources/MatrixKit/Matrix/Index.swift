//
//  MatrixIndex.swift
//  
//
//  Created by Artem Myshkin on 18.07.2021.
//

import Foundation

public struct Index {

    public var row: Int
    public var column: Int

    public init(
        row: Int,
        column: Int
    ) {
        self.row = row
        self.column = column
    }

    internal init(
        offset: Int,
        consider size: Size
    ) {
        self.init(
            row: offset / size.columns,
            column: offset % size.columns
        )
    }

}

extension Index: Comparable {

    public static func < (lhs: Self, rhs: Self) -> Bool {
        if lhs == rhs {
            return false
        } else if lhs.row < rhs.row {
            return true
        } else if lhs.row == rhs.row && lhs.column < rhs.column {
            return true
        } else {
            return false
        }
    }

}

extension Index: Hashable {

}

extension Index: Equatable {

    public static func ==(lgs: Self, rgs: Self) -> Bool {
        return lgs.column == rgs.column && lgs.row == rgs.row
    }

}

extension Index: CustomStringConvertible {

    public var description: String {
        return "(row: \(row), column: \(column))"
    }

}
