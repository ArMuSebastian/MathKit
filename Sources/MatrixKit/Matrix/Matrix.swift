//
//  Matrix.swift
//  
//
//  Created by Artem Myshkin on 18.07.2021.
//

import Foundation
import ConsoleDrawKit

public struct Matrix<GenericElement> {

    public typealias TwoDimensionalArray<T> = [[T]]
    public typealias Payload = TwoDimensionalArray<Element>

    public typealias Element = GenericElement
    public typealias Index = MatrixKit.Index

    private var _elements: [Index: Element]
    private var _indices: [Index]
    private var _size: Size

}

// MARK: - Inits
extension Matrix {

    public init?(
        array: [[Element]]
    ) {
        guard array.isValidMatrixPayload
        else {
            return nil
        }

        self.init(
            elements: array.flatted,
            size: .init(
                rows: array.count,
                columns: array.first!.count
            )
        )
    }

    public init?(
        array: [Element],
        amountOfRows: Int
    ) {
        guard
            amountOfRows > .zero,
            array.count > .zero,
            array.count.isMultiple(of: amountOfRows)
        else {
            return nil
        }
        self.init(
            elements: array,
            size: .init(
                rows: amountOfRows,
                columns: array.count / amountOfRows
            )
        )
    }

    public init?(
        array: [Element],
        amountOfColumns: Int
    ) {
        guard
            amountOfColumns > .zero,
            array.count > .zero,
            array.count.isMultiple(of: amountOfColumns)
        else {
            return nil
        }
        self.init(
            elements: array,
            size: .init(
                rows: array.count / amountOfColumns,
                columns: amountOfColumns
            )
        )
    }

    private init(
        elements: [Element],
        size: Size
    ) {
        let indexedElements: [(Matrix<Element>.Index, Element)] = elements
            .enumerated()
            .map { pair in
                let (offset, element) = pair
                return (Index.init(offset: offset, consider: size), element)
            }

        self.init(
            indexedElements: indexedElements,
            size: size
        )
    }

    private init(
        indexedElements: [(index: Matrix<Element>.Index, element: Element)],
        size: Size
    ) {
        self._indices = indexedElements.map(\.index)
        self._elements = Dictionary(uniqueKeysWithValues: indexedElements)
        self._size = size
    }

}

// MARK: - Subscripts
extension Matrix {

    private func safeElement(by index: Index) -> Element? {
        self[safe: index]
    }

    private subscript(safe index: Index) -> Element? {
        get {
            self._elements[index]
        }
        set {
            if self.indices.contains(index) {
                self._elements[index] = newValue
            }
        }
    }

    public subscript(index: Index) -> Element {
        get {
            self._elements[index]!
        }
        set {
            if !self.indices.contains(index) {
                fatalError("Unable to set value for index \(index) that is not contaibed bby martix size \(self.size)")
            }
            self._elements[index] = newValue
        }
    }

    public subscript(indices: [Index]) -> [Element] {
        indices
            .map(self.safeElement(by:))
            .map(\.unsafelyUnwrapped)
    }

    public subscript(safe indices: [Index]) -> [Element] {
        indices
            .compactMap(self.safeElement(by:))
    }

}

// MARK: - Index
extension Matrix {

    public var size: Size {
        self._size
    }

    public var indices: [Index] {
        self._indices
    }

    public var startIndex: Index {
        return self.indices.first ?? Index(row: 0, column: 0)
    }

    public var endIndex: Index {
        return self.indices.last ?? Index(row: 0, column: 0)
    }


    public func index(after i: Index) -> Index {
        return _index(after: i)!
    }

    private func _index(after i: Index) -> Index? {
        let nextIndex: Index?
        if i.column < self.size.endIndex.column {
            nextIndex = Index(
                row: i.row,
                column: i.column + 1
            )
        } else if i.row < self.size.endIndex.row {
            nextIndex = Index(
                row: i.row + 1,
                column: self.size.startIndex.column
            )
        } else {
            nextIndex = nil
        }
        return nextIndex
    }

    public func index(before i: Index) -> Index {
        return _index(before: i)!
    }

    public func _index(before i: Index) -> Index? {
        let prevIndex: Index?
        if i.column > self.size.startIndex.column {
            prevIndex = Index(
                row: i.row,
                column: i.column - 1
            )
        } else if i.row > self.size.startIndex.row {
            prevIndex = Index(
                row: i.row - 1,
                column: self.size.endIndex.column
            )
        } else {
            prevIndex = nil
        }
        return prevIndex
    }

}

extension Matrix: Equatable where GenericElement: Equatable {

    public static func ==(lhs: Self, rhs: Self) -> Bool {
        lhs._elements == rhs._elements
    }

}

extension Matrix: Hashable where Element: Hashable {

    public func hash(into hasher: inout Hasher) {
        hasher.combine(self._elements)
    }

}

// MARK: - CustomStringConvertible
extension Matrix: CustomStringConvertible {

    public var description: String {
        let elements = self[self.indices]
            .map(String.init(describing:))
            .chunked(by: self.size.columns)

        return ConsoleDrawKit.Table(
            accessory: Table.Accessory(
                headerAccessory: .number,
                assideAccessory: .number,
                divideAccessory: .backslash
            ),
            characterSet: Table.DrawingCharacterSet.second
        )
            .string(from: elements)
    }

}
