//
//  Step.swift
//  
//
//  Created by Artem Myshkin on 03.08.2021.
//

import Foundation

extension Axis.Direction {

    public struct Step {

        public let horisontal: Int
        public let vertical: Int

        public init(
            horisontal: Int,
            vertical: Int
        ) {
            self.horisontal = horisontal
            self.vertical = vertical
        }

        static
        public func * (
            directionDelta: Self,
            multiplier: Int
        ) -> Self {
            return Self(
                horisontal: directionDelta.horisontal * multiplier,
                vertical: directionDelta.vertical * multiplier
            )
        }

        static
        public func * (
            multiplier: Int,
            directionDelta: Self
        ) -> Self {
            return Self(
                horisontal: directionDelta.horisontal * multiplier,
                vertical: directionDelta.vertical * multiplier
            )
        }

    }

}
